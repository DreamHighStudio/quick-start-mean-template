import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let LocationModels = new Schema({
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true,
    },
    notes:{
        type: String,
    }

}, {
    timestamp: true
});
const Location = mongoose.model('Location', LocationModels);
module.exports = Location;
