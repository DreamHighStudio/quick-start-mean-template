import mongoose from 'mongoose';
import Location from './locations-models';
import Waiver from './waivers-models'

const Schema = mongoose.Schema;

let EventsModels = new Schema({
    class_type: {
        type: String,
        enum: ['CLASSES', 'EVENT', 'ONE-ON-ONE', 'SERIES'],
        required: true
    },
    instructors: {
        type: [],
        default: [],
    },
    location: {
        type: [Location],
        default: [],
    },
    waivers: {
        type: [Waiver],
        default: []
    },
    max_attendance: {
        type: Number
    },
    period: {
        type: String,
        enum: ['REPEATING', 'ONE-OFF']
    },
    start_time: {
        type: Date,
    },
    end_time: {
        type: Date
    },
    //I think this not needed
    repeat_forever: {
        type: Boolean,
        default:false

    },
    duration: {
        type: Date
    },
    days: {
        type: String,
        enum: ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY']
    },
    tags: {
        type: [String],
        default:[]
    },
    display_app: {
        type: Boolean,
        default:false
    },
    publish:{
        type: Boolean,
        default: false
    }

}, {
    timestamp: true
});
const Events = mongoose.model('Events', EventsModels);
module.exports = Events;
