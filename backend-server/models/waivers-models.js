import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let WaiverModels = new Schema({
    title: {
        type: String,
        required: true
    },
    waiver: {
        type: String,
        required: true,
    }

}, {
    timestamp: true
});
const Waiver = mongoose.model('Waiver', WaiverModels);
module.exports = Waiver;
