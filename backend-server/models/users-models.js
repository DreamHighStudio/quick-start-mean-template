import mongoose from 'mongoose';
import bcrypt from "bcryptjs";

const Schema = mongoose.Schema;

let UsersModels = new Schema({
    username: {
        type: String,
        required: true
    },
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone_num: {
        type: Number,
    },
    gender: {
        type: String,
    },
    date_of_birth: {
        type: Date,
    },
    ethnicity: {
        type: String,
    },
    display_pic:{
        data:Buffer,
        contentType: String
    },
    tags: {
        type: [],
        default: []
    },
    // timestamps: {
    //     createdAt: 'createdAt',
    //     updatedAt: 'updatedAt'
    // },
    // verified: {
    //     type: Boolean,
    //     default: false
    // },
    // accountType: {
    //     type: String,
    //     enum: ['professor', 'student'],
    // }


});
const Users = mongoose.model('Users', UsersModels);
module.exports = Users;
module.exports.hashPassword = async (password) => {
    try {
        const salt = await bcrypt.genSalt(10);
        return await bcrypt.hash(password, salt);
    } catch (error) {
        throw new Error(error);
    }
};

module.exports.comparePasswords = async (inputPassword, hashedPassword) =>{
  try {
      return await bcrypt.compare(inputPassword, hashedPassword);
  }  catch (error) {
      throw new Error(error);

  }
};
