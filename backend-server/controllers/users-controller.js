import Users from '../models/users-models'

const jwt = require('jsonwebtoken');
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const SECRET = 'RnwetTiV5bars+A33aIjzR1F4ZZGcVA%xsjd%G7U%qnnnz#SfdsFfsfe342';
const passportOpts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: SECRET
};

passport.use(new JwtStrategy(passportOpts, function (jwtPayload, done) {
    const expirationDate = new Date(jwtPayload.exp * 1000);
    if (expirationDate < new Date()) {
        return done(null, false);
    }
    done(null, jwtPayload);
}));

passport.serializeUser(function (user, done) {
    done(null, user.username);
});

async function login(req, res, next) {

    const {username, password} = req.body;
    const user = {
        'username': username,
    };
    let isValid = await authenticateUser(username, password);
    if (isValid) {
        const token = await jwt.sign(user, SECRET, {expiresIn: 3600000});
        res.json({jwt: token});
    } else {
        // Error code 1002 auth failed.
        res.json(1002);
    }
}

function logout(req, res, next) {
    res.sendStatus(204);
}

async function signUp(req, res, next) {
    await Users.findOne({username: req.body.username}, async (err, result) => {
        if (result != null) {
            // Error code 1001 duplicate account found associated with this account.
            res.json(1001)
        } else {
            const temp_user = {
                username: 'admin',
                password: '123456',
                first_name: 'Bob',
                last_name: 'Martin',
                email: 'freedom@gmail.com'
            }
            let hash = await Users.hashPassword(temp_user.password);
            let user = new Users();
            user.username = temp_user.username;
            user.first_name = temp_user.first_name;
            user.last_name = temp_user.last_name;
            user.email = temp_user.email;
            user.password = hash;
            // user.username = req.body.username;
            // user.first_name = req.body.first_name;
            // user.last_name = req.body.last_name;
            // user.email = req.body.email;
            // user.password = hash;
            user.save().then(user => {
                res.json(202);
            }).catch(err => {
                res.status(400).send("Failed to create new record");
            });
        }
    });
}

function dashbored(req, res, next) {
    // console.log("dashbored");
    res.json("dashbored works");
}


async function authenticateUser(username, password) {
    let isValid = false;
    await Users.findOne({username: username}, async (err, result) => {
        if (result != null) {
            await Users.comparePasswords(password, result.password).then((res) => {
                if (res) {
                    isValid = true;
                }
            });
        }
    });
    return isValid;
}


module.exports = {
    signUp,
    login,
    logout,
    dashbored
};
