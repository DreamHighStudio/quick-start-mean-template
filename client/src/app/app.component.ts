import {Component} from '@angular/core';
import {Router, NavigationEnd} from "@angular/router";
import construct = Reflect.construct;
import {UserService} from "./services/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'Monmouth Fitness Center';
  showHeaderFooter: boolean;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if(event instanceof NavigationEnd){
        this.showHeaderFooter = (event.url !== '/login');
      }
    });
  }

}
