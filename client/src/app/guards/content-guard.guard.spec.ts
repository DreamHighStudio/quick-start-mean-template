import { TestBed, async, inject } from '@angular/core/testing';

import { ContentGuardGuard } from './content-guard.guard';

describe('ContentGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContentGuardGuard]
    });
  });

  it('should ...', inject([ContentGuardGuard], (guard: ContentGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
