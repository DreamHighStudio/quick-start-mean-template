import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginComponent} from './components/login/login.component';
import {DashboredComponent} from './components/dashboard/dashbored.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";

import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';

import {UserService} from './services/user.service'
import {ValidateService} from "./services/validate.service";
import {TokenInterceptor} from "./providers/token-interceptor";
import {ContentGuardGuard} from "./guards/content-guard.guard";
import {AuthGuard} from "./guards/auth.guard";

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { CommonModule } from '@angular/common';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FlatpickrModule } from 'angularx-flatpickr';
import 'flatpickr/dist/flatpickr.css';
import { AddToCalendarModelComponent } from './components/live-schedule/add-to-calendar-model/add-to-calendar-model.component';
import { CreateClassSessionModelComponent } from './components/live-schedule/create-class-session-model/create-class-session-model.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboredComponent,
    HeaderComponent,
    FooterComponent,
    AddToCalendarModelComponent,
    CreateClassSessionModelComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule,
    NgbModule,
    CommonModule,
    CalendarModule.forRoot({provide: DateAdapter, useFactory: adapterFactory}),
    FlatpickrModule.forRoot()
  ],
  providers: [
    UserService,
    ValidateService,
    ContentGuardGuard,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
