import {Component, OnInit} from '@angular/core';
import {UserService} from "../../services/user.service";
import {NavigationEnd, Router} from "@angular/router";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    isActive = "active";
    username: string;
  showHeaderFooter: boolean;
    constructor(private userService: UserService, private router: Router) {
      // Needed for when the page is redirected
      this.showHeaderFooter = this.userService.isLoggedIn()
      // Needed for when the page is navigated in this case the page is not recreated abut an event happens
      this.router.events.subscribe((event) => {
        if(event instanceof NavigationEnd){
          this.showHeaderFooter = (event.url !== '/login');
        }
      });
    }

    ngOnInit() {
        this.username = this.userService.getUser();
    }

    toggleMenu() {
      if (this.isActive == ""){
        this.isActive = "active";
      }else{
          this.isActive = "";
      }
    }

    logout(){
      this.userService.logout();
    }
}
