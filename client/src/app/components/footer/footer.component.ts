import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  showHeaderFooter: boolean;

  constructor(private userService: UserService, private router: Router) {
    // Needed for when the page is redirected
    this.showHeaderFooter = this.userService.isLoggedIn()
    // Needed for when the page is navigated in this case the page is not recreated abut an event happens
    this.router.events.subscribe((event) => {
      console.log(event)
      if (event instanceof NavigationEnd) {
        this.showHeaderFooter = (event.url !== '/login');
      }
    });
  }

  ngOnInit() {
  }

}
