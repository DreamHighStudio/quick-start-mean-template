import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    username: string;
    password: string;

    // login_form: any;

    hide_login_invalid: boolean = true;
    errMsg = "";

    constructor(private userService: UserService, private router: Router) {
    }

    ngOnInit() {
        // this.login_form = new FormGroup({
        //   email: new FormControl(this.email, [
        //     Validators.required,
        //     //Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$"),
        //   ]),
        //   password: new FormControl(this.password, [
        //     Validators.required,
        //   ]),
        // })
    }

    login() {
        this.userService.login(this.username, this.password).subscribe((res) => {
            if (res == '1002') {
                this.hide_login_invalid = false;
                this.errMsg = "Please check the username and password";
            } else {
                this.router.navigate(['/dashboard']);
                this.hide_login_invalid = true;
                this.errMsg = "";
            }
        });
    }

}
