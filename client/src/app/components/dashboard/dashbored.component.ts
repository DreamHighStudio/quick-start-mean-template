import {Component, OnInit} from '@angular/core';
import {config} from "../../../config";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashbored',
  templateUrl: './dashbored.component.html',
  styleUrls: ['./dashbored.component.scss']
})
export class DashboredComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) {
  }

  ngOnInit() {

  }

  liveSchedule() {
    this.router.navigate(["/live-schedule"])
  }

}
