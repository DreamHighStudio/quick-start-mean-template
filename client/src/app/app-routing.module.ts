import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from "./components/login/login.component";
import {DashboredComponent} from "./components/dashboard/dashbored.component";
import {AuthGuard} from "./guards/auth.guard";
import {ContentGuardGuard} from "./guards/content-guard.guard";



const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'dashboard',
    component: DashboredComponent,
    canActivate:[ContentGuardGuard],
    canLoad: [ContentGuardGuard],
  },
  // {path:'page-not-found', component:PageNotFoundComponent},
  // {path: '**', redirectTo: 'page-not-found'},
  {path: '', redirectTo: 'login', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
